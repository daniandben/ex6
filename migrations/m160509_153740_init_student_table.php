<?php

use yii\db\Migration;

class m160509_153740_init_student_table extends Migration
{
     public function up()
    {
		$this->createTable(
		'student',
			[
				'id' => 'pk',
				'name' => 'string'
				
			]
		);
    }

    public function down()
    {
		$this->dropTable('student');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
