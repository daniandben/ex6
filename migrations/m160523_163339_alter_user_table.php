<?php

use yii\db\Migration;

class m160523_163339_alter_user_table extends Migration
{
    public function up()
    {
		$this->addColumn('user','firstname','string');
		$this->addColumn('user','lastname','string');
		$this->addColumn('user','email','string');
		$this->addColumn('user','phone','string');
		$this->addColumn('user','created_at','integer');
		$this->addColumn('user','updated_at','integer');
		$this->addColumn('user','created_by','integer');
		$this->addColumn('user','updated_by','integer');
		
    }

    public function down()
    {
        echo "m160523_163339_alter_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
